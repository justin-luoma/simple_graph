use simple_graph::graph::Graph;

fn main() {
    let mut graph: Graph<&str> = Graph::new();
    graph.add_vertex("Bob");
    graph.add_vertex("Alice");
    graph.add_vertex("Mark");
    graph.add_vertex("Rob");
    graph.add_vertex("Maria");
    graph.add_edge("Bob", "Alice");
    graph.add_edge("Bob", "Rob");
    graph.add_edge("Alice", "Mark");
    graph.add_edge("Rob", "Mark");
    graph.add_edge("Alice", "Maria");
    graph.add_edge("Rob", "Maria");

    graph.print();

    println!("{:?}", graph.dfs("Bob"));
    println!("{:?}", graph.bfs("Bob"));
}
