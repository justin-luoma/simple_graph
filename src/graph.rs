use std::collections::{HashMap, LinkedList};
use std::fmt::Display;
use std::hash::Hash;

pub struct Graph<T> {
    vertices: HashMap<T, LinkedList<T>>,
}

impl<T: Clone + Display + Eq + Hash + Ord> Graph<T> {
    pub fn new() -> Self {
        Self {
            vertices: HashMap::new(),
        }
    }

    pub fn add_vertex(&mut self, vertex: T) {
        self.vertices.insert(vertex, LinkedList::new());
    }

    pub fn add_edge(&mut self, source: T, target: T) {
        if !self.vertices.contains_key(&source) {
            self.vertices
                .insert(source.clone(), LinkedList::from([target.clone()]));
        } else {
            let mut children = self.vertices.remove(&source).unwrap();
            children.push_back(target.clone());
            self.vertices.insert(source.clone(), children);
        }
        if !self.vertices.contains_key(&target) {
            self.vertices
                .insert(target.clone(), LinkedList::from([source]));
        } else {
            let mut children = self.vertices.remove(&target).unwrap();
            children.push_back(source);
            self.vertices.insert(target, children);
        }
    }

    pub fn print(&self) {
        print!("{: <10}\t", "");
        let mut cols: Vec<T> = self.vertices.clone().into_keys().collect();
        cols.sort();
        for vertex in cols.clone() {
            print!("{: <10}\t", vertex);
        }
        println!();
        for row_vertex in cols.clone() {
            let row_adj = self.vertices.get(&row_vertex).unwrap();
            print!("{: <10}\t", row_vertex);
            for col_vertex in cols.clone() {
                print!(
                    "{: <10}\t",
                    if row_adj.contains(&col_vertex) { 1 } else { 0 }
                );
            }
            println!();
        }
    }

    pub fn dfs(&self, root: T) -> LinkedList<T> {
        let mut visited: LinkedList<T> = LinkedList::new();
        let mut stack = LinkedList::from([root]);
        while !stack.is_empty() {
            let vertex = stack.pop_back().unwrap();
            if !visited.contains(&vertex) {
                visited.push_back(vertex.clone());
                for v in self.vertices.get(&vertex).unwrap() {
                    stack.push_back(v.to_owned());
                }
            }
        }

        visited
    }

    pub fn bfs(&self, root: T) -> LinkedList<T> {
        let mut visited: LinkedList<T> = LinkedList::new();
        let mut queue = LinkedList::from([root]);
        while !queue.is_empty() {
            let vertex = queue.pop_front().unwrap();
            if !visited.contains(&vertex) {
                visited.push_back(vertex.clone());
                for v in self.vertices.get(&vertex).unwrap() {
                    queue.push_back(v.to_owned());
                }
            }
        }

        visited
    }
}
